<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Affiliate List</title>
  </head>
  <body>

    <h2>All Affiliates</h2>
    <p>The following is the full list of Affiliates</p>

    @foreach ($allcontacts as $c)
      {{ $c }} <br>
    @endforeach

    <h2>Affiliates nearby</h2>
    <p>The following is a list of Affiliates living within 100km</p>

    @foreach ($nearbycontacts as $c)
      {{ $c }} <br>
    @endforeach

  </body>
</html>
