<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{

  /**
    * Display list of Affiliates
  */
  public function show()
  {
    $data = self::getAffiliateList('affiliates');
    $affiliateNames = array(
      'data' => $data
    );

    $affiliateSLCoords = array();
    $affiliateSLNames = array();

    // tmp affiliate arrays due to issue with input file
    $affiliateLats = array(
      52.986375, 51.92893, 51.8856167, 52.3191841, 53.807778, 53.4692815,
      54.0894797, 53.038056, 54.1225, 53.1229599, 52.2559432, 52.240382,
      53.2451022, 53.1302756, 53.008769, 53.1489345, 53, 51.999447,
      52.966, 52.366037, 54.180238, 53.0033946, 52.228056, 54.133333,
      55.033, 53.521111, 51.802, 54.374208, 53.74452, 53.761389,
      54.080556, 52.833502
    );

    $affiliateLongs = array(
      -6.043701, -10.27699, -10.4240951, -8.5072391, -7.714444, -9.436036,
       -6.18671, -7.653889, -8.143333, -6.2705202, -7.1048927,
       -6.972413, -6.238335, -6.2397222, -6.1056711, -6.8422408,
       -7, -9.742744, -6.463, -8.179118, -5.920898, -6.3877505,
       -7.915833, -6.433333, -8.112, -9.831111, -9.442, -8.371639,
       -7.11167, -7.2875, -6.361944, -8.522366
    );

    // contacts list
    $affiliateContacts = array(
      "affiliate_id: 12,  name: Yosef Giles",
      "affiliate_id: 1, name: Lance Keith",
      "affiliate_id: 2, name: Mohamed Bradshaw",
      "affiliate_id: 3, name: Rudi Palmer",
      "affiliate_id: 28, name: Macsen Freeman",
      "affiliate_id: 7, name: Mikaeel Fenton",
      "affiliate_id: 8, name: Addison Lister",
      "affiliate_id: 26, name: Moesha Bateman",
      "affiliate_id: 27, name: Roan Estes",
      "affiliate_id: 6, name: Jez Greene",
      "affiliate_id: 9, name: Macaulay Melia",
      "affiliate_id: 10, name: Maja Blevins",
      "affiliate_id: 4, name: Inez Blair",
      "affiliate_id: 5, name: Sharna Marriott",
      "affiliate_id: 11, name: Isla-Rose Hubbard",
      "affiliate_id: 31, name: Maisha Mccarty",
      "affiliate_id: 13, name: Terence Wall",
      "affiliate_id: 14, name: Zakariyah Bean",
      "affiliate_id: 15, name: Veronica Haines",
      "affiliate_id: 16, name: Linzi Carver",
      "affiliate_id: 17, name: Gino Partridge",
      "affiliate_id: 39, name: Kirandeep Browning",
      "affiliate_id: 18, name: Gwen Rankin",
      "affiliate_id: 24, name: Ellena Olson",
      "affiliate_id: 19, name: Aleisha Hayward",
      "affiliate_id: 20, name: Hadiya Terrell",
      "affiliate_id: 21, name: Keanu Oliver",
      "affiliate_id: 22, name: Shayna Potts",
      "affiliate_id: 29, name: Alvin Stamp",
      "affiliate_id: 30, name: Kingsley Vang",
      "affiliate_id: 23, name: Ciara Bannister",
      "affiliate_id: 25, name: Tasneem Wolfe",
    );

    for($i = 0; $i < count($affiliateLats); $i++){
      for($j = 0; $j < count($affiliateLongs); $j++){
        $distance = self::calculateDistance($affiliateLats[$i],$affiliateLongs[$j]);
        if($distance < 100){
          // array_push($affiliateSLCoords, $affiliateLats[$i],$affiliateLongs[$j]);
          array_push($affiliateSLNames, $affiliateContacts[$i]);
        }

      }
    }
    $affiliateSLNames = array_unique($affiliateSLNames);
    return view('home', ['allcontacts' => $affiliateContacts], ['nearbycontacts' => $affiliateSLNames]);

  }


  /**
    * Get list of affiliates from JSON file in storage
  */
  private function getAffiliateList($filename)
  {
    $path = storage_path() . "/json/$filename.txt";
    // $file = fopen($path, "r") or die("Unable to open file");
    // $fileContents = fread($file, filesize($path));
    $fileContents = json_decode(file_get_contents($path), true);

    return $fileContents;
  }

  /**
  * calculate distance from Dublin office to affiliates address
  */
  private function calculateDistance($affiliateLat,$affiliateLong)
  {
    $r = 6372.795477598;

    // Dublin office address co-ordinates
    $homeLat = 53.3340285;
    $homeLong = -6.2535495;

    // convert degrees to radians
    $homeLong = deg2rad($homeLong);
    $homeLat = deg2rad($homeLat);
    $affiliateLong = deg2rad($affiliateLong);
    $affiliateLat = deg2rad($affiliateLat);

    $deltaLat = $affiliateLat - $homeLat;
    $deltaLon = $affiliateLong - $homeLong;

    $angle = 2 * asin(sqrt(pow(sin($deltaLat / 2), 2) + cos($homeLat) * cos($affiliateLat) * pow(sin($deltaLon / 2), 2)));

    return round($angle * $r, 2);

  }


}
